Feature: Testing of main items on Book Club

  Scenario: Transition to the Book page
    Given user opens main page
    When user clicks "Книги" top menu item
    Then user sees "Купить книги в нашем интернет-магазине" page title
    And user sees "Главная » Книги" in the breadcrumb

  Scenario: Transition to the Shopping cart page
    Given user opens main page
    When user clicks "Корзина" item
    Then user sees "Ваша «Корзина»" cart page title
    And user sees "Главная » Корзина" in the breadcrumb

  Scenario: Transition to the Goods page
    Given user opens main page
    When user clicks "Товары" top menu item
    Then user sees "Товары от Книжного Клуба" page title
    And user sees "Главная » Товары" in the breadcrumb

  Scenario: Search on main page
    Given user opens main page
    When user types "психология" to the search form
    And user presses the search button
    Then user sees relevant word "психология" in the searching results

  Scenario: Transition to the main page
    Given user opens main page
    When user clicks "Серии" top menu item
    Then user sees "Серии книг" page title
    And user sees "Главная » Серии" in the breadcrumb
    When user clicks on main logo
    Then user sees "«Книжный Клуб «Клуб Семейного Досуга»" page title

  Scenario: Login on main page
    Given user opens main page
    When user presses login icon
    And user types "3916179" to the card number field and types "Рощупкина" to the surname field
    Then user sees "Мой кабинет" in the menu user button

  Scenario Outline: Validate language switcher
    Given user opens main page
    When user presses "<language>" selector
    Then user sees "<label>" page title
    Examples:
      | language | label                                     |
      | УКР      | «Книжковий Клуб «Клуб Сімейного Дозвілля» |
      | УКР РУС  | «Книжный Клуб «Клуб Семейного Досуга»     |

  Scenario: Transition to the news page
    Given user opens main page
    When user presses button news
    Then user sees "Новости Клуба" page title
