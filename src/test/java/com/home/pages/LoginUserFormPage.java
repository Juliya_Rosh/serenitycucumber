package com.home.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

import java.util.Set;

/**
 * Created by Julia on 15.06.2017.
 */
public class LoginUserFormPage extends PageObject {

    public void typeToTheCardNumberField(String cardNumber, String surname) {
        WebElementFacade form = $(".enterinsite>form");
        form.findElement(By.id("nncField")).sendKeys(cardNumber);
        form.findElement(By.id("snameField")).sendKeys(surname);
        form.findElement(By.xpath("//div[3]/input")).click();
    }
}
