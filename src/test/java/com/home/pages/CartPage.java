package com.home.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class CartPage extends PageObject {

    @FindBy(id = "txt_your_cart")
    private WebElementFacade cartPageTitle;

    public String getPageTitleText(){
        return cartPageTitle.getText();
    }
}
