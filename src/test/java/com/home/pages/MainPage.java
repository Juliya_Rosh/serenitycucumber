package com.home.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.NoSuchElementException;

import java.util.List;


public class MainPage extends PageObject {

    @FindBy(xpath = ".//*[@id='mainMenuRoot']/li/a")
    private List<WebElementFacade> topMenuItemsList;

    public void clickOnTheTopMenuItem(String itemName) {
        for (WebElementFacade webElement : topMenuItemsList) {
            if (itemName.equals(webElement.getText())){
                webElement.click();
                return;
            }
        }
        throw new NoSuchElementException("Element "+itemName+" not found");
    }
}
