package com.home.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.NoSuchElementException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class CommonElementsPage extends PageObject {

    @FindBy(xpath = ".//*[@id='allcontent']//h1")
    private WebElementFacade pageTitle;

    @FindBy(xpath = ".//*[@id='bread_crumbs']//tr/td")
    private WebElementFacade breadCrumb;

    @FindBy(xpath = ".//*[@id='head2']//div[3]/a/span")
    private WebElementFacade cartItem;

    @FindBy(xpath = ".//*[@id='head2']//form/input[1]")
    private WebElementFacade searchForm;

    @FindBy(xpath = ".//*[@id='head2']//form/input[2]")
    private WebElementFacade searchButton;

    @FindBy(xpath = ".//*[@id='allcontent']//table/tbody/tr/td[2]/div[2]/a")
    private List<WebElementFacade> searchResults;

    @FindBy(xpath = ".//*[@id='logo']//img")
    private WebElementFacade mainLogo;

    @FindBy(xpath = ".//*[@id='head2']/div[2]/div[1]/a/span")
    private WebElementFacade loginIcon;

    @FindBy(xpath = ".//*[@id='head2']//div[1]/a/span")
    private WebElementFacade userMenuButton;

    @FindBy(xpath = ".//*[@id='language']/div/a")
    private WebElementFacade activeLanguageSelector;

    @FindBy(xpath = ".//*[@id='allcontent']/section/section/aside/div[1]/div[1]/a")
    private WebElementFacade battonNews;

    public String getTitleName() {
        return pageTitle.getText();
    }

    public String getBreadcrumbText() {
        return breadCrumb.getText();
    }

    public void clickCartItem() {
        cartItem.click();
    }

    public void typeToTheSearchForm(String query) {
        searchForm.type(query);
    }

    public void pressTheSearchButton() {
        searchButton.click();
    }

    public List<String> getSearchResultsText() {
        return searchResults.stream()
                .map(WebElementFacade::getText).collect(Collectors.toList());
    }

    public void clicksOnMainLogo() {
        mainLogo.click();
    }

    public void clickLoginIcon() {
        loginIcon.click();
    }

    public String getUserMenuLabelText() {
        return userMenuButton.getText();
    }

    public void clickOnLanguageSelector(String language) {
        if (language.equals(activeLanguageSelector.getText())) {
            activeLanguageSelector.click();
        } else {
            throw new NoSuchElementException("Element " + language + " is active");
        }
    }

    public void clickOnButtonNews() {
        battonNews.click();
    }
}
