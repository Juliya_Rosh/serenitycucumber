package com.home.steps;

import com.home.steps.serenity.MainPageSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;


public class MainPageDefinitionSteps {

    @Steps
    private MainPageSteps mainPageSteps;



    @Given("^user opens main page$")
    public void userOpensMainPage() {
        mainPageSteps.openMainPage();
    }

    @When("^user clicks \"([^\"]*)\" top menu item$")
    public void userClicksTopMenuItem(String itemName) throws Throwable {
        mainPageSteps.clickOnTheTopMenuItem(itemName);
    }

}
