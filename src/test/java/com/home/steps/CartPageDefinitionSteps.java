package com.home.steps;

import com.home.steps.serenity.CartPageSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class CartPageDefinitionSteps {

    @Steps
    private CartPageSteps cartPageSteps;

    @Then("^user sees \"([^\"]*)\" cart page title$")
    public void userSeesCartPageTitle(String pageTile) throws Throwable {
        cartPageSteps.checkCartPageTitle(pageTile);
    }

}
