package com.home.steps;

import com.home.steps.serenity.CommonElementsSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;


public class CommonElementsDefinitionSteps {

    @Steps
    private CommonElementsSteps commonElementsSteps;

    @Given("^user sees \"([^\"]*)\" page title$")
    public void userSeesNameOfPageTitle(String titleName){
        commonElementsSteps.checkNameOfPageTitle(titleName);
    }

    @And("^user sees \"([^\"]*)\" in the breadcrumb$")
    public void userSeesInTheBreadcrumb(String breadcrumbText) throws Throwable {
        commonElementsSteps.checkBreadcrumb(breadcrumbText);
    }

    @When("^user clicks \"Корзина\" item$")
    public void userClicksCartItem() throws Throwable {
        commonElementsSteps.clickCartItem();
    }

    @When("^user types \"([^\"]*)\" to the search form$")
    public void userTypesToTheSearchForm(String query) throws Throwable {
        commonElementsSteps.typeToTheSearchForm(query);

    }

    @And("^user presses the search button$")
    public void userPressesTheSearchButton() throws Throwable {
        commonElementsSteps.pressTheSearchButton();
    }

    @Then("^user sees relevant word \"([^\"]*)\" in the searching results$")
    public void userSeesRelevantWordInTheSearchingResults(String result) throws Throwable {
        commonElementsSteps.checkRelevantWordInTheSearchingResults(result);
    }

    @When("^user clicks on main logo$")
    public void userClicksOnMainLogo() throws Throwable {
        commonElementsSteps.clicksOnMainLogo();

    }

    @When("^user presses login icon$")
    public void userPressesLoginIcon() throws Throwable {
        commonElementsSteps.clickOnLoginIcon();
    }


    @Then("^user sees \"(.*)\" in the menu user button$")
    public void userSeesInTheMenuUserButton(String expectedResult) throws Throwable {
        commonElementsSteps.checkInTheMenuUserButton(expectedResult);
    }

    @When("^user presses \"([^\"]*)\" selector$")
    public void userPressesSelector(String languages) throws Throwable {
        commonElementsSteps.clickOnLanguageSelector(languages);
    }

    @When("^user presses button news$")
    public void userPressesButtonNews() throws Throwable {
        commonElementsSteps.clickOnButtonNews();
    }
}
