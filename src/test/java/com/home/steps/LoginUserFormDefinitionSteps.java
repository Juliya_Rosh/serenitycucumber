package com.home.steps;

import com.home.steps.serenity.LoginUserFormSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

/**
 * Created by Julia on 15.06.2017.
 */
public class LoginUserFormDefinitionSteps {

    @Steps
    private LoginUserFormSteps loginUserFormSteps;

    @And("^user types \"([^\"]*)\" to the card number field and types \"([^\"]*)\" to the surname field$")
    public void userTypesToTheCardNumberField(String cardNumber, String surname) throws Throwable {
        loginUserFormSteps.typeToTheCardNumberField(cardNumber, surname);
    }
}
