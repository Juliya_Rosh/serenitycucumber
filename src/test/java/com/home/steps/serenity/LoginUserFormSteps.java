package com.home.steps.serenity;

import com.home.pages.LoginUserFormPage;
import net.thucydides.core.annotations.Step;

/**
 * Created by Julia on 15.06.2017.
 */
public class LoginUserFormSteps{

    private LoginUserFormPage loginUserFormPage;

    @Step
    public void typeToTheCardNumberField(String cardNumber, String surname) {
        loginUserFormPage.typeToTheCardNumberField(cardNumber, surname);
    }
}
