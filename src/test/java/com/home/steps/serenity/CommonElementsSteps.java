package com.home.steps.serenity;

import static org.assertj.core.api.Assertions.*;

import com.home.pages.CommonElementsPage;
import net.thucydides.core.annotations.Step;

import java.util.List;


public class CommonElementsSteps {

    private CommonElementsPage commonElementsPage;

    @Step
    public void checkNameOfPageTitle(String titleName) {
        assertThat(commonElementsPage.getTitleName()).as("Page title is wrong").isEqualTo(titleName);
    }

    @Step
    public void checkBreadcrumb(String breadcrumbText) {
        assertThat(commonElementsPage.getBreadcrumbText()).as("BreadCrumb value is wrong")
                .isEqualTo(breadcrumbText);
    }

    @Step
    public void clickCartItem() {
        commonElementsPage.clickCartItem();
    }

    @Step
    public void typeToTheSearchForm(String query) {
        commonElementsPage.typeToTheSearchForm(query);
    }

    @Step
    public void pressTheSearchButton() {
        commonElementsPage.pressTheSearchButton();

    }

    @Step
    public void checkRelevantWordInTheSearchingResults(String result) {
        List<String> searchResults = commonElementsPage.getSearchResultsText();
        for (String searchResult : searchResults) {
            assertThat(searchResult.toLowerCase()).as("Search result does not contain " + result)
                    .contains(result);
        }
    }

    @Step
    public void clicksOnMainLogo() {
        commonElementsPage.clicksOnMainLogo();
    }

    @Step
    public void clickOnLoginIcon() {
        commonElementsPage.clickLoginIcon();
    }

    @Step
    public void checkInTheMenuUserButton(String expectedResult) {
        String actualResult = commonElementsPage.getUserMenuLabelText();
        assertThat(actualResult).as("User menu label text is not matched").isEqualTo(expectedResult);
    }

    @Step
    public void clickOnLanguageSelector(String languages) {
        String[] language = languages.split(" ");
        for (String s : language) {
            commonElementsPage.clickOnLanguageSelector(s);
        }

    }

    public void clickOnButtonNews() {
        commonElementsPage.clickOnButtonNews();
    }
}
