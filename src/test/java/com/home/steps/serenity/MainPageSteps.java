package com.home.steps.serenity;

import com.home.pages.MainPage;
import net.thucydides.core.annotations.Step;


public class MainPageSteps {

    private MainPage mainPage;

    @Step
    public void openMainPage() {
        mainPage.getDriver().get("https://www.bookclub.ua/");
    }

    @Step
    public void clickOnTheTopMenuItem(String itemName) {
        mainPage.clickOnTheTopMenuItem(itemName);
    }
}
