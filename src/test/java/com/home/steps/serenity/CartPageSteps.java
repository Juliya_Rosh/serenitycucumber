package com.home.steps.serenity;

import com.home.pages.CartPage;
import net.thucydides.core.annotations.Step;
import static org.assertj.core.api.Assertions.*;


public class CartPageSteps {

    private CartPage cartPage;

    @Step
    public void checkCartPageTitle(String pageTile) {
      assertThat(cartPage.getPageTitleText()).as("Title is not matched").isEqualTo(pageTile);
    }
}
